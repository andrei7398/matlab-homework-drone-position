DeltaT = 0.65; % - de calculat mean DeltaT_k - %
A = [1 DeltaT; 0 1];
B = [DeltaT^2/2; DeltaT];
C = [1 0; 0 1];

K = place(A,B,[-0.1 -0.2]);
A = A - B * K;

x0 =  [x_mavlink_local_position_ned_t(end); vx_mavlink_local_position_ned_t(end)];
R  = 0.1;
a  = 1;

N = length(x_mavlink_local_position_ned_t);
x(:,1) = x0;
for k=1:N
    x(:,k+1) = A * x(:,k) + B * a;
    y(:,k) = C * x(:,k) + rand(2,1) * sqrt(R);
end

subplot(2,1,1);
plot(y(1,:));
title('Pozitia');
xlabel('timp');
ylabel('pozitie');

subplot(2,1,2);
plot(y(2,:));
title('Viteza');
xlabel('timp');
ylabel('viteza');


