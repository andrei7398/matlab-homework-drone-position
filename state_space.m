DeltaT = 0.65; % - de calculat mean DeltaT_k - %
A = [1 DeltaT; 0 1];
B = [DeltaT^2/2; DeltaT];
C = [1 0];

x0 = [x_mavlink_local_position_ned_t(1); vx_mavlink_local_position_ned_t(1)];
R  = 0.1;
a  = xacc_mavlink_raw_imu_t;

N = length(x_mavlink_local_position_ned_t);
x(:,1) = x0; 
for k=1:N
    %%  in cazul variant, matricile A si B
    %% trebuiesc calculate la fiecare iteratie
    x(:,k+1) = A * x(:,k) + B * a(k);
    y(:,k) = C * x(:,k) + rand(1) * sqrt(R);
end

subplot(2,1,1);
plot(y);
hold on;
plot(x_mavlink_local_position_ned_t);
title('Pozitia');
xlabel('timp');
ylabel('pozitie');

subplot(2,1,2);
plot(x(2,:));
hold on;
plot(vx_mavlink_local_position_ned_t);
title('Viteza');
xlabel('timp');
ylabel('viteza');
